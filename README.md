This a project where i built a web app using angular8 as a frontend technology and nodejs as a backend technology.
For the porpose of this demo each component is running on a seperate docker container where the RESTApis are running in a nodejs service consumed in a simple angular app.

To run this app you will need `docker` + `docker-compose` installed in your system then 
follow these instructions:

1.  `git clone git@gitlab.com:wertanimehdi1/projet_module_tp.git`
2.  `docker-compose up` 
3.  now visit your localhost