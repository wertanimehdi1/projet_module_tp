import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  greet = 'please wait...';
  constructor() {
    this.getMessage();
  }
  getMessage() {
    fetch('http://localhost:8000/')
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        this.greet = data.msg;
      })
  }
}
